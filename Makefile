SHELL := sh
.SHELLFLAGS := -o errexit -c
.DEFAULT_GOAL := all
.DELETE_ON_ERROR:
.SUFFIXES:
.ONESHELL:

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
project_dir := $(dir $(mkfile_path))
project_slug := react
slugname := redbuttontrainer
image_name := "$(slugname)-$(project_slug)"

CONTAINER_RUNTIME := docker

source_files := Containerfile $(shell find . -type f ! -path './dist/*' ! -path './.dist-*' ! -path './Containerfile')

HASH := $(shell echo "$(source_files)" | xargs -n1 md5sum | sort | md5sum - | cut -d' ' -f1)

objects := dist/$(image_name).tar .dist-$(HASH)

# For debugging what is set in variables.
inspect.%:
	@printf "%s" '$($*)'

# Always run.  Useful when target is like targetname.% .
# Use $* to get the stem
FORCE:

.PHONY: all
all: $(objects) ## Default is to make a container image

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: dist
dist: .dist-$(HASH) dist/$(image_name).tar ## Create container image for distribution

.dist-$(HASH) dist/$(image_name).tar &: $(source_files)
	find . -empty -path './.dist-*' -delete
	find dist/ -depth -mindepth 1 -type f ! -name '.gitkeep' -delete
	find dist/ -depth -mindepth 1 -type d -empty -delete
	$(CONTAINER_RUNTIME) build \
		-f "$(project_dir)Containerfile" \
		--tag localhost/$(image_name):latest \
		"$(project_dir)"
	$(CONTAINER_RUNTIME) save -o dist/$(image_name).tar localhost/$(image_name):latest
	touch .dist-$(HASH)
	du -h dist/$(image_name).tar

.PHONY: static
static: dist/static/index.html ## Create static files for distribution

dist/static/index.html: dist/$(image_name).tar .dist-$(HASH)
	$(CONTAINER_RUNTIME) load --input $<
	$(CONTAINER_RUNTIME) create --rm --quiet --name $(image_name) localhost/$(image_name):latest
	$(CONTAINER_RUNTIME) cp $(image_name):/home/$(slugname)/static dist/
	$(CONTAINER_RUNTIME) container rm $(image_name)
	$(CONTAINER_RUNTIME) image rm localhost/$(image_name):latest

.PHONY: dev
dev: ## Start local development container with only a bind mount of src directory
	tmp_iidfile="$$(mktemp)"
	$(CONTAINER_RUNTIME) build \
		--iidfile "$$tmp_iidfile" \
		--target "dev" \
		-f "$(project_dir)Containerfile" \
		"$(project_dir)"
	dev_image_name="$$(cat "$$tmp_iidfile")"
	rm -f "$$tmp_iidfile"
	$(CONTAINER_RUNTIME) run \
		-it \
		--rm \
		-p 38081:38081 \
		--mount "type=bind,src=$(project_dir)src,dst=/home/dev/app/src,readonly" \
		"$$dev_image_name" || printf ""
	$(CONTAINER_RUNTIME) image rm "$$dev_image_name"

.PHONY: start
start: ## Start container
	tmp_iidfile="$$(mktemp)"
	$(CONTAINER_RUNTIME) build \
		--iidfile "$$tmp_iidfile" \
		-f "$(project_dir)Containerfile" \
		"$(project_dir)"
	dev_image_name="$$(cat "$$tmp_iidfile")"
	rm -f "$$tmp_iidfile"
	$(CONTAINER_RUNTIME) run \
		-it \
		--rm \
		-p 38081:38081 \
		"$$dev_image_name" || printf ""
	$(CONTAINER_RUNTIME) image rm "$$dev_image_name"

.PHONY: clean
clean: ## Remove files that were created
	find dist/ -depth -mindepth 1 -type f ! -name '.gitkeep' -delete
	find dist/ -depth -mindepth 1 -type d -empty -delete

.PHONY: upkeep
upkeep: ## Send to stderr any upkeep comments that have a past due date
	@grep -r -n --exclude-dir='.git/' -E "^\W+UPKEEP\W+(due:\W?\".*?\"|label:\W?\".*?\"|interval:\W?\".*?\")" . \
	| xargs -L 1 \
	python -c "\
import sys; \
import datetime; \
import re; \
now=datetime.date.today(); \
upkeep=\" \".join(sys.argv[1:]); \
m=re.search(r'due: (\d{4}-\d{2}-\d{2})', upkeep); \
due=datetime.date.fromisoformat(m.group(1)); \
remaining=due - now; \
sys.exit(upkeep if remaining.days < 0 else 0)"
