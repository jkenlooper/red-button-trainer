# Red Button Trainer

> Production grade red button training for business critical applications. Train
> your workers for real life scenarios involving red button encounters that
> simulate preventing disasters when acted upon quickly.

![Example Results for the Red Button Trainer](public/example-result.png "Example Results")

Red Button Trainer is really an example project that is being developed by Jake
Hickenlooper to try out various things in a very limited scope. The limited
scope for this is:

- Display a red button.
- Signal the user to press the button at a random time up to 13 seconds.
- Show the results to the user.

This is currently being implemented as a React application that uses web
components from [Wokwi Elements](https://github.com/wokwi/wokwi-elements). The
development environment is isolated to a container (docker or podman).

## Development

Use [Vite](https://vite.dev/) to build and hot reload the website on any changes
to source files. The website will be available at: http://localhost:38081/ after
it builds a development container image and runs it.

```bash
# Build and run the website in development mode.
make dev
```

## Deployment

Can be deployed as a container or upload the files to a static file server.

```bash
# Create dist/redbuttontrainer-react.tar container archive.
make

# Extract files to dist/static/ directory.
make static
```

## License

Red Button Trainer. Copyright (C) 2025 Jake Hickenlooper

[GNU Affero General Public License](https://choosealicense.com/licenses/agpl-3.0/).

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

## Contributing

Please contact Jake Hickenlooper or create an issue.

Any submitted changes to this project require the commits to be signed off with
the
[git command option '--signoff'](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff).
This ensures that the committer has the rights to submit the changes under the
project's license and agrees to the
[Developer Certificate of Origin](https://developercertificate.org).
