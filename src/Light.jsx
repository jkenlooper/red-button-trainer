import PropTypes from "prop-types";

function Light({ label, value, color }) {
  return (
    <div>
      <span className="u-hiddenVisually">
        {color} light {label} is {value ? "on" : "off"}.
      </span>
      <wokwi-led
        aria-hidden="true"
        value={value ? "1" : ""}
        label={label}
        color={color}
      >
      </wokwi-led>
    </div>
  );
}

Light.propTypes = {
  label: PropTypes.string,
  value: PropTypes.bool,
  color: PropTypes.string,
};

export default Light;
