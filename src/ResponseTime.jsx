import PropTypes from "prop-types";
import "./ResponseTime.css";
import { digitTo7Segment } from "./digit-to-7segment.js";

function ResponseTime({ value }) {
  const status = (() => {
    if (value === undefined) {
      return "waiting";
    }
    if (value === -1) {
      return "error";
    }
    return "done";
  })();
  const outputContent = (() => {
    let el;
    let values;
    switch (status) {
      case "waiting":
        values = [].concat(
          digitTo7Segment("-", true),
          digitTo7Segment("-", false),
          digitTo7Segment("-", false),
          digitTo7Segment("-", false),
        );
        el = (
          <wokwi-7segment
            digits={values.length / 8}
            color="yellow"
            pins="extend"
            offColor="#222"
            values={values}
          >
          </wokwi-7segment>
        );
        break;
      case "error":
        values = [].concat(
          digitTo7Segment("f"),
          digitTo7Segment("a"),
          digitTo7Segment("i"),
          digitTo7Segment("l"),
        );
        el = (
          <wokwi-7segment
            digits={values.length / 8}
            color="yellow"
            pins="extend"
            offColor="#222"
            values={values}
          >
          </wokwi-7segment>
        );
        break;
      case "done":
        values = [].concat(
          ...value.split("").reduceRight(
            (acc, char) => {
              if (char === ".") {
                acc.dec = true;
                return acc;
              }
              acc.segments.unshift(digitTo7Segment(char, acc.dec));
              acc.dec = false;
              return acc;
            },
            { dec: false, segments: [] },
          ).segments,
        );
        el = (
          <time className="u-block" dateTime={"PT" + value + "S"}>
            <wokwi-7segment
              digits={values.length / 8}
              color="yellow"
              pins="extend"
              offColor="#222"
              values={values}
            >
            </wokwi-7segment>
          </time>
        );
        break;
      default:
        el = "";
    }
    return el;
  })();
  return (
    <div className="ResponseTime">
      <output
        name="response-time"
        htmlFor="red-button"
        className="ResponseTime-output"
      >
        {outputContent}
      </output>
    </div>
  );
}
ResponseTime.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
};

export default ResponseTime;
