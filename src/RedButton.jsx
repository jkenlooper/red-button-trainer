import PropTypes from "prop-types";
function RedButton({ onClick }) {
  return (
    <div>
      <wokwi-pushbutton
        color="red"
        onClick={onClick}
      >
      </wokwi-pushbutton>
    </div>
  );
}

RedButton.propTypes = {
  onClick: PropTypes.func,
};
export default RedButton;
