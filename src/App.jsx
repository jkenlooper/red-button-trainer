import "suitcss-utils-display";
import "./App.css";
import RedButtonTrainer from "./RedButtonTrainer.jsx";

function App() {
  return <RedButtonTrainer></RedButtonTrainer>;
}

export default App;
