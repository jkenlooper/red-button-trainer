import PropTypes from "prop-types";
import "./LightArray.css";
import Light from "./Light.jsx";

const lightsDefinition = {
  lightBulbs: [
    {
      color: "red",
    },
    {
      color: "green",
    },
  ],
};

function LightArray({ lights }) {
  const _lights = lights
    .reduce((acc, lightValue, index) => {
      const light = {
        id: index,
        value: lightValue,
        label: `l${index + 1}`,
        color: lightsDefinition.lightBulbs[index].color,
      };
      acc.push(light);
      return acc;
    }, [])
    .map((light) => {
      return (
        <li key={light.id} className="LightArray-listItem">
          <Light
            label={light.label}
            value={light.value}
            color={light.color}
          >
          </Light>
        </li>
      );
    });

  return (
    <div className="LightArray">
      <ul className="LightArray-list" aria-live="polite">
        {_lights}
      </ul>
    </div>
  );
}

LightArray.propTypes = {
  lights: PropTypes.arrayOf(PropTypes.bool),
};

export default LightArray;
