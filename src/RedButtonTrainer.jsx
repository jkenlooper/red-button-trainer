import { useState } from "react";

import "./RedButtonTrainer.css";
import RedButton from "./RedButton.jsx";
import LightArray from "./LightArray.jsx";
import ResponseTime from "./ResponseTime.jsx";

const states = Object.freeze({
  init: "init",
  up: "up",
  ready: "ready",
  down: "down",
  fail: "fail",
});

function RedButtonTrainer() {
  const [redButtonTrainerState, setRedButtonTrainerState] = useState(
    states.init,
  );
  const [start, setStart] = useState(undefined);
  const [stop, setStop] = useState(undefined);
  const [lights, setLights] = useState([false, true]);
  const [randomTimeout, setRandomTimeout] = useState(undefined);

  if (redButtonTrainerState === states.init) {
    setRedButtonTrainerState(states.up);
    setLights([true, false]);
    setRandomTimeout((() => {
      const startDelay = 3 * 1000;
      const maxTimeout = 7 * 1000;
      const randomDelay = Math.round(startDelay + Math.random() * maxTimeout);

      return window.setTimeout(() => {
        setLights([false, true]);
        setStart(Date.now());
        setRedButtonTrainerState(states.ready);
      }, randomDelay);
    })());
  }

  function handleRedButtonClick() {
    switch (redButtonTrainerState) {
      case states.ready:
        // show results
        setStop(Date.now());
        setRedButtonTrainerState(states.down);
        setLights([false, false]);
        break;
      case states.down:
        // reset
        setStop(undefined);
        setStart(undefined);
        setRedButtonTrainerState(states.init);
        break;
      case states.up:
        // clicked too soon
        window.clearTimeout(randomTimeout);
        setStop(undefined);
        setStart(undefined);
        setRedButtonTrainerState(states.fail);
        setLights([false, false]);
        break;
      default:
        break;
    }
  }

  const roundedResponseTime = (() => {
    if (redButtonTrainerState === states.fail) {
      return -1;
    } else {
      return start && stop ? getRoundedResponseTime(start, stop) : undefined;
    }
  })();

  return (
    <div className="RedButtonTrainer">
      <div className="RedButtonTrainer-display">
        <LightArray lights={lights}></LightArray>
      </div>

      <div className="RedButtonTrainer-output">
        <ResponseTime value={roundedResponseTime}></ResponseTime>
      </div>

      <div className="RedButtonTrainer-console">
        <RedButton onClick={handleRedButtonClick} />
      </div>
    </div>
  );
}

function getRoundedResponseTime(start, stop) {
  /* Return a rounded time that will fit in 4 characters (not counting
   * the decimal point). This is so the 7 segment component will be able
   * to show the value.
   */
  const actual = Math.min(9999, (stop - start) / 1000);
  const wholeSeconds = actual - (actual % 1);
  const zeroPaddedFraction = ((actual % 1).toString() + ".0").split(".")[1] +
    "000";
  const rounded = `${wholeSeconds.toString()}.${zeroPaddedFraction}`.slice(
    0,
    actual > 1000 ? 4 : 5,
  );
  return rounded;
}

export default RedButtonTrainer;
