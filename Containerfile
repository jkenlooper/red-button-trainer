# UPKEEP due: "2025-05-01" label: "Node image" interval: "+3 months"
# docker pull registry.hub.docker.com/library/node:22.13-bookworm
# docker image ls --digests node
FROM registry.hub.docker.com/library/node:22.13-bookworm@sha256:ae2f3d4cc65d251352eca01ba668824f651a2ee4d2a37e2efb22649521a483fd AS build

ARG DEBIAN_FRONTEND=noninteractive
RUN echo "Create dev user"; \
  groupadd -g 44444 dev && useradd -u 44444 -g dev -s /bin/sh --create-home dev

WORKDIR /home/dev/app

RUN echo "Install packages"; \
  apt-get update --yes  \
  && apt-get upgrade --yes

# Very untrusting of npm here.
RUN echo "Create empty node_modules dir"; \
  mkdir /home/dev/app/node_modules \
  && mkdir /home/dev/app/dist \
  && touch /home/dev/app/package-lock.json \
  && chown dev:dev /home/dev/app/node_modules \
  && chown dev:dev /home/dev/app/dist \
  && chown dev:dev /home/dev/app/package-lock.json

COPY --chown=dev:dev package.json /home/dev/app/

USER dev

RUN echo "Build node_modules, package-lock.json"; \
  npm install

COPY --chown=dev:dev public/ /home/dev/app/public/
COPY --chown=dev:dev src/ /home/dev/app/src/
COPY --chown=dev:dev index.html /home/dev/app/
COPY --chown=dev:dev vite.config.js /home/dev/app/

RUN echo "Build dist"; \
  npm run build

CMD ["sh", "-c", "while true; do printf 'z'; sleep 60; done"]

# UPKEEP due: "2025-05-01" label: "Node image" interval: "+3 months"
# docker pull registry.hub.docker.com/library/node:22.13-bookworm
# docker image ls --digests node
FROM registry.hub.docker.com/library/node:22.13-bookworm@sha256:ae2f3d4cc65d251352eca01ba668824f651a2ee4d2a37e2efb22649521a483fd AS dev

ARG DEBIAN_FRONTEND=noninteractive
RUN echo "Create dev user"; \
  groupadd -g 44444 dev && useradd -u 44444 -g dev -s /bin/sh --create-home dev

WORKDIR /home/dev/app

RUN echo "Create data storage directory"; \
  mkdir -p /var/lib/redbuttontrainer-datastore/ \
  && chown -R dev:dev /var/lib/redbuttontrainer-datastore/

RUN echo "Install packages"; \
  apt-get update --yes \
  && apt-get upgrade --yes

ENV PORT=38081
ENV BIND=0.0.0.0

COPY --chown=dev:dev --from=build /home/dev/app/package-lock.json /home/dev/app/
COPY --chown=dev:dev --from=build /home/dev/app/package.json /home/dev/app/
COPY --chown=dev:dev --from=build /home/dev/app/node_modules /home/dev/app/node_modules

USER dev

RUN echo "npm clean install"; \
  npm ci

COPY --chown=dev:dev eslint.config.js /home/dev/app/
COPY --chown=dev:dev --from=build /home/dev/app/public/ /home/dev/app/public/
COPY --chown=dev:dev --from=build /home/dev/app/src/ /home/dev/app/src/
COPY --chown=dev:dev --from=build /home/dev/app/index.html /home/dev/app/
COPY --chown=dev:dev --from=build /home/dev/app/vite.config.js /home/dev/app/

RUN echo "Linting code"; \
  npm run lint

CMD ["npm", "run", "dev"]

# UPKEEP due: "2025-03-03" label: "busybox" interval: "+3 months"
# https://hub.docker.com/_/busybox
FROM registry.hub.docker.com/library/busybox:1.36.1-musl@sha256:d097373897633648934dce99c57a4ddfc9b31872c64c5bd35ea986fc47a53fcb AS app

# The site conf file should also have a SLUGNAME_UID=10008 env entry.
RUN echo "Create redbuttontrainer user"; \
  adduser -D -u "10008" "redbuttontrainer"

COPY --chown=redbuttontrainer:redbuttontrainer --from=build /home/dev/app/dist /home/redbuttontrainer/static/

USER redbuttontrainer

CMD ["httpd", "-f", "-p", "0.0.0.0:38081", "-h", "/home/redbuttontrainer/static", "-v"]
